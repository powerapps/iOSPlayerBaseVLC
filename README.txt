This is a project example for Free Video Collect Plus which is a powerful video player for iOS: 
https://itunes.apple.com/us/app/free-video-collect-plus-for/id662860598?mt=8

Free Video Collect Plus develops based on the open source project of VLC.This Demo can play most of audio formats,includes network audio and video streams. We write this for iOS developers who want to add powerful video player to their app using a couple lines of code,and they also should comply with the open source VLC licensed under Mozilla Public License Version 2.

Note

Although the SDK supports 1080p video, but we suggest you play 1080p video only on iPad Air & iPhone 5s devices.

Compile Requirements

ARC
XCode 5.1 & iOS SDK 7.0

Deploy Requirements

ARMv7, ARMv7s, ARM64, i386 and x86-64 architectures
Deploy target iOS 5.1

Features

ARC support.
armv7, armv7s, arm64, i386 and x86-64 support.
Local file support.
Network file support.
Main Boudle file support.
http and rtmp protocols support.
Parse audio and video duration.
Grab video thumbnails.
Real-time bit & frame rate and network buffering progress calculation.
Query current playback time info.
Playback speed control.
Brightness, contrast and saturation control.
Background audio, video playback support.
Full screen mode support.
Play from a specified time position.
Play, pause, resume, stop, fast forward, fast backward, seek position actions support.
Delegate support, you can get notification when state, playback progress, buffering progress changed and enter/exit full screen mode.
Multiple audio, subtitle streams support.

Dolby License

DO NOT use dolby tech in your iOS app unless you have a dolby license. Dolby Digital(AC3), Dolby Digital Plus(E-AC3) and Dolby TrueHD(MLP).

Phone: +86 18627201588
Mail: freevideodler@gmail.com